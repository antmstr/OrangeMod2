package com.athomas.restwsnonjpa.DAO;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.athomas.restwsnonjpa.Helper.UserAssist;
import com.athomas.restwsnonjpa.entities.Team;
import com.athomas.restwsnonjpa.entities.User;

@Repository
public class UserDAOImpl implements UserDAO {

	@PersistenceContext
	EntityManager em;

	@Override
	public List<UserAssist> getUsers() {

		List<UserAssist> returns = new <UserAssist>ArrayList();

		List<User> users = em.createQuery("from User").getResultList();
		for (User user : users) {
			UserAssist uas = new UserAssist();
			uas.setId(user.getId());
			uas.setFname(user.getFirstName());
			uas.setLname(user.getLastName());
			uas.setDeleted(user.isDeleted());
			uas.setEmail(user.getEmail());
			uas.setPhone(user.getPhone());
			uas.setLocation(user.getLocation());
			uas.setCompanyId(user.getCompany_name());
			uas.setPosition(user.getPosition());

			List<Team> teams = user.getTeams();
			for (Team team : teams) {
				if (team.getParentTeam() != null) {
					// uas.addParentIds(team.getParentTeam());
					// uas.addSubTeamIds(team.getId());
				} else {
					// uas.addParentIds(team.getId());
				}
			}

			returns.add(uas);
		}

		return returns;

	}

	@Override
	public UserAssist getUser(String userid) {

		List<UserAssist> returns = new <UserAssist>ArrayList();

		User user = (User) em.createQuery("from User u WHERE u.id in ('" + userid + "')").getSingleResult();
		if (user != null) {
			UserAssist uas = new UserAssist();
			uas.setId(user.getId());
			uas.setFname(user.getFirstName());
			uas.setLname(user.getLastName());
			uas.setDeleted(user.isDeleted());
			uas.setEmail(user.getEmail());
			uas.setPhone(user.getPhone());
			uas.setLocation(user.getLocation());
			uas.setCompanyId(user.getCompany_name());
			uas.setPosition(user.getPosition());

			List<Team> teams = user.getTeams();
			for (Team team : teams) {
				if (team.getParentTeam() == null) {
					// uas.addParentIds(team.getId());
				} else {
					// uas.addSubTeamIds(team.getId());
				}
			}

			returns.add(uas);
		}
		if (returns.size() == 0)
			return null;
		UserAssist ua = returns.get(0);
		if (ua == null) {
			return null;
		}
		if (ua.isDeleted()) {
			return null;
		}
		return ua;

	}

	public UserAssist createUser(String firstName, String lastName, String email, String phone, String location,
			Integer company_name, String position, Integer subTeamId) {

		Team team = null;

		User user = new User();
		user.setFirstName(firstName);
		user.setLastName(lastName);
		user.setEmail(email);
		user.setPhone(phone);
		user.setLocation(location);
		user.setCompany_name(company_name);
		user.setPosition(position);
		if (subTeamId != null) {
			team = em.find(Team.class, subTeamId);
			user.addTeams(team);
		}
		user.setDeleted(false);
		System.out.println("About to persist!");
		em.persist(user);

		UserAssist uas = new UserAssist();
		uas.setId(user.getId());
		uas.setFname(user.getFirstName());
		uas.setLname(user.getLastName());
		uas.setDeleted(user.isDeleted());
		uas.setEmail(user.getEmail());
		uas.setPhone(user.getPhone());
		uas.setLocation(user.getLocation());
		if (subTeamId != null) {
			// uas.addSubTeamIds(subTeamId);
		}
		uas.setCompanyId(user.getCompany_name());
		uas.setPosition(user.getPosition());
		return uas;
	}

	@Override
	public void updateUser(String id, String firstName, String lastName, String email, String phone, String location,
			Integer company_name, String position, Integer subTeamId) {
		Team team;

		User user = em.find(User.class, UUID.fromString(id));
		System.out.println("before adding team " + user.getTeams().size());
		// em.getTransaction().begin();
		if ((firstName != null) && (!firstName.isEmpty())) {
			user.setFirstName(firstName);
		}
		if ((lastName != null) && (!lastName.isEmpty())) {
			user.setLastName(lastName);
		}
		if ((email != null) && (!email.isEmpty())) {
			user.setEmail(email);
		}
		if ((phone != null) && (!phone.isEmpty())) {
			user.setPhone(phone);
		}
		if ((location != null) && (!location.isEmpty())) {
			user.setLocation(location);
		}
		if ((company_name != null)) {
			user.setCompany_name(company_name);
		}
		if ((position != null) && (!position.isEmpty())) {
			user.setPosition(position);
		}
		if (subTeamId != null) {
			team = em.find(Team.class, subTeamId);
			user.addTeams(team);
		}
		System.out.println("after adding team " + user.getTeams().size());
		List<Team> tt = user.getTeams();
		for (Team t : tt) {
			System.out.println("One is " + t.getId());
		}
		em.persist(user);

	}

	@Override
	public User deleteUser(String id) {
		User user = em.find(User.class, UUID.fromString(id));
		user.setDeleted(true);
		em.persist(user);
		return null;
	}

}
