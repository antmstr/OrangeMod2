package com.athomas.restwsnonjpa.DAO;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import com.athomas.restwsnonjpa.entities.Company;

@Repository
public class CompanyDAOImpl implements CompanyDAO {

	@PersistenceContext
	EntityManager em;

	@Override
	public List<Company> getCompanies() {
		List<Company> companies = em.createQuery("from Company").getResultList();
		return companies;
	}

	@Override
	public Company getCompany(Integer companyId) {
		Company company = (Company) em.createQuery("from Company where id in ('" + companyId + "')").getSingleResult();
				
		return company;
	}

}
