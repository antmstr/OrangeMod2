package com.athomas.restwsnonjpa.DAO;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import com.athomas.restwsnonjpa.entities.Team;

@Repository
public class TeamDAOImpl implements TeamDAO {

	@PersistenceContext
	EntityManager em;

	@Override
	public List<Team> getTeams() {
		List<Team> teams = em.createQuery("from Team").getResultList();
		return teams;
	}

	@Override
	public Team getTeam(Integer id) {
		Team team = (Team) em.createQuery("from Team where id in ('" + id + "')").getSingleResult();
		return team;
	}

}
