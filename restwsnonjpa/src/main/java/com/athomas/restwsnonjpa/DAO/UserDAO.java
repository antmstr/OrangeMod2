package com.athomas.restwsnonjpa.DAO;

import java.util.List;

import com.athomas.restwsnonjpa.Helper.UserAssist;
import com.athomas.restwsnonjpa.entities.User;

public interface UserDAO {

	public List<UserAssist> getUsers();

	public UserAssist getUser(String id);

	public UserAssist createUser(String firstName, String lastName, String email, String phone, String location,
			Integer company_name, String position, Integer subTeamId);

	public User deleteUser(String id);

	public void updateUser(String id, String firstName, String lastName, String email, String phone, String location,
			Integer company_name, String position, Integer subTeamId);
}
