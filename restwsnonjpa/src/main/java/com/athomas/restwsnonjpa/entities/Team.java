package com.athomas.restwsnonjpa.entities;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import org.springframework.web.context.annotation.RequestScope;

@Entity
@Table(name = "TEAM")
public class Team {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private int id;

	@Column(name = "name")
	private String name;

	@Column(name = "parent_team")
	private Integer parentTeam;

	@Column(name = "deleted")
	boolean deleted;
	
	/*@ManyToMany(fetch = FetchType.LAZY, cascade = { CascadeType.PERSIST, CascadeType.DETACH, CascadeType.DETACH,
			CascadeType.REFRESH })
	@JoinTable(name = "team_membership", joinColumns = @JoinColumn(name = "subteam_id"), inverseJoinColumns = @JoinColumn(name = "user_id"))
	private List<User> users;*/


	public Team() {

	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getParentTeam() {
		return parentTeam;
	}

	public void setParentTeam(Integer parentTeam) {
		this.parentTeam = parentTeam;
	}

	public boolean isDeleted() {
		return deleted;
	}

	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}

	/*public List<User> getUsers() {
		return users;
	}

	public void setUsers(List<User> users) {
		this.users = users;
	}*/

	@Override
	public String toString() {
		return "Team [id=" + id + ", name=" + name + ", parentTeam=" + parentTeam + ", deleted=" + deleted  + "]";
	}

	

}
