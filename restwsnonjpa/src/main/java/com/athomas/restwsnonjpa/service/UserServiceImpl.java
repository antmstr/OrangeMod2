package com.athomas.restwsnonjpa.service;

import java.util.HashMap;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestParam;

import com.athomas.restwsnonjpa.DAO.UserDAO;
import com.athomas.restwsnonjpa.Helper.UserAssist;
import com.athomas.restwsnonjpa.entities.User;
import com.google.gson.Gson;

@Transactional
@Service
public class UserServiceImpl implements UsersService {

	@Autowired
	UserDAO userDAO;

	private HashMap<String, User> users = new HashMap<>();

	@Override
	public String getUsers() {
		List<UserAssist> users = userDAO.getUsers();
		String json = new Gson().toJson(users);
		return json;
	}

	@Override
	public String getUser(String id) {
		UserAssist user = userDAO.getUser(id);
		String json = new Gson().toJson(user);
		return json;
	}

	@Override
	public String createUser(String firstName, String lastName, String email, String phone, String location,
			Integer company_name, String position, Integer subTeamId) {

		UserAssist user = userDAO.createUser(firstName, lastName, email, phone, location, company_name, position, subTeamId);
		String json = new Gson().toJson(user);
		return json;
	}

	@Override
	public void updateUser(String id, String firstName, String lastName, String email, String phone, String location,
			Integer company_name, String position, Integer subTeamId) {
		System.out.println("id in updateUseer is " + id);
		System.out.println("firstName in updateUseer is " + firstName);
		userDAO.updateUser(id, firstName, lastName, email, phone, location, company_name, position,subTeamId);

	}

	@Override
	public String deleteUser(String id) {
		System.out.println(" Service the id is " + id);
		User user = userDAO.deleteUser(id);
		String json = new Gson().toJson(user);
		return json;
	}

}
