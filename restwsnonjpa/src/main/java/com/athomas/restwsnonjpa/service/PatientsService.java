package com.athomas.restwsnonjpa.service;

import java.util.List;


import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.athomas.restwsnonjpa.model.Patient;

@RestController
public interface PatientsService {

	@CrossOrigin(origins = "*", allowedHeaders = "*")
	@RequestMapping(value = "/patients", method = RequestMethod.GET, produces = "application/json")
	public List<Patient> getPatients();
	
	
}



