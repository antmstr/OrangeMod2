package com.athomas.restwsnonjpa.Helper;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class UserAssist {

	private UUID id;
	private String fname;
	private String lname;
	private String email;
	private String phone;
	private String location;
	private String position;
	private Integer companyId;
	// private List<Integer> parentIds;
	// private List<Integer> subTeamIds;
	private boolean deleted;

	public UserAssist() {

	}

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public String getFname() {
		return fname;
	}

	public void setFname(String fname) {
		this.fname = fname;
	}

	public String getLname() {
		return lname;
	}

	public void setLname(String lname) {
		this.lname = lname;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public Integer getCompanyId() {
		return companyId;
	}

	public void setCompanyId(Integer companyId) {
		this.companyId = companyId;
	}

	public boolean isDeleted() {
		return deleted;
	}

	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	/*
	 * public List<Integer> getParentIds() { return parentIds; }
	 * 
	 * public void setParentIds(List<Integer> parentIds) { this.parentIds =
	 * parentIds; }
	 * 
	 * public List<Integer> getSubTeamIds() { return subTeamIds; }
	 * 
	 * public void setSubTeamIds(List<Integer> subTeamIds) { this.subTeamIds =
	 * subTeamIds; }
	 * 
	 * public void addSubTeamIds(Integer stid) { if (subTeamIds == null) {
	 * subTeamIds = new ArrayList<Integer>(); } subTeamIds.add(stid); }
	 * 
	 * public void addParentIds(Integer pid) { if (parentIds == null) { parentIds =
	 * new ArrayList<Integer>(); } parentIds.add(pid); }
	 */

}
